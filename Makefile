all: test-suggestion test-elastic test-suggestion-buffer

OBJECTS = \
	egg-animation.o \
	egg-frame-source.o \
	egg-suggestion-entry.o \
	egg-suggestion-entry-buffer.o \
	egg-suggestion-popover.o \
	egg-suggestion-row.o \
	egg-suggestion.o \
	egg-elastic-bin.o \
	fuzzy.o \
	resources.o

resources.stamp: egg.gresource.xml $(shell glib-compile-resources --generate-dependencies egg.gresource.xml)
	@ echo " [GEN]   $@" && \
	glib-compile-resources --generate-source --target resources.c egg.gresource.xml; \
	glib-compile-resources --generate-header --target resources.h egg.gresource.xml; \
	touch resources.stamp

resources.c: resources.stamp
resources.h: resources.stamp

PKGS = gtk+-3.0 webkit2gtk-4.0

test-elastic: $(OBJECTS) test-elastic.c
	@ echo " [CCLD]  $@" && \
	$(CC) -o $@ -Wall -o $@ -ggdb -O0 $(OBJECTS) $(shell pkg-config --cflags --libs $(PKGS)) test-elastic.c

test-suggestion: $(OBJECTS) test-suggestion.c
	@ echo " [CCLD]  $@" && \
	$(CC) -o $@ -Wall -o $@ -ggdb -O0 $(OBJECTS) $(shell pkg-config --cflags --libs $(PKGS)) test-suggestion.c

test-suggestion-buffer: $(OBJECTS) test-suggestion-buffer.c
	@ echo " [CCLD]  $@" && \
	$(CC) -o $@ -Wall -o $@ -ggdb -O0 $(OBJECTS) $(shell pkg-config --cflags --libs $(PKGS)) test-suggestion-buffer.c

%.o: %.c %.h
	@ echo " [CC]    $@" && \
	$(CC) -c -o $@ -Wall -o $@ -ggdb -O0 $*.c $(shell pkg-config --cflags $(PKGS))


clean:
	rm -f test-suggestion resources.{c,h,stamp} *.o

